<?php

namespace App\Controller;


use App\Form\UserType;
use App\Security\MessageDigestPasswordEncoder;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use App\Model\UserQuery;
use App\Model\AdmanaQuery;

use App\Model\User;
use App\Model\Admana;
use Symfony\Component\HttpFoundation\Response;



class HomeController extends Controller
{
    /**
     * @Route("/", name="home")
     */
    public function indexAction(Request $request)
    {
        $posts=UserQuery::create()
        ->findOne();

        $set=new User();
       $set->setUsername('user');
       $set->setPassword('mark');
        $posts->save();

        // //$set1=new Admana();
        // $users->setFirstname('Suraj');
        // $users->save();



        return $this->render('user/sidebar.html.twig', [
            'posts'=>$posts
        ]);
    }

    /**
     * @Route("/password", name="change_password")
     */
    public function passwordAction(Request $request)
    {
        $user = $this->getUser();

        $form = $this->createForm(
            UserType::class,
            $user
        );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $userManager = $this->container->get('fos_user.user_manager');
            $userManager->updateUser($user);

            $this->get('session')->getFlashBag()->add(
                'success',
                'Password was successfully updated.'
            );

            return $this->redirect($this->generateUrl('change_password'));
        }

        return $this->render('user/password.html.twig', [
            'form' => $form->createView()
        ]);

    }
    /**
     * @Route("/list", name="user_list")
     */
    public function listAction(Request $request)
    {
       $query=$this->getQuery();

       $posts=$query->find();

       return $this->render("user/datatable.html.twig",['posts'=>$posts]);
    }

     protected function getQuery()
    {
        return UserQuery::create();
    }


    /**
     * @Route("/user/{id}/delete", name="user_delete")
     * @throws \Exception
     */
    public function deleteAction(Request $request, $id)
    {
        $user = $this->getUsers($id);
        $user->delete();

        $this->get('session')->getFlashBag()->add(
            'success',
            'User deleted successfully'
        );

        return $this->redirect($this->generateUrl('user_list'));
    }

    public function getUsers($id)
    {
        $user = UserQuery::create()
            ->findPk($id);

        if (!$user) {
            throw new \Exception('No user detail found');
        }

        return $user;
    }
    /**
     * @Route("/user/{id}/edit", name="user_edit")
     * @throws \Exception
     */
    public function editAction(Request $request, $id)
    {
            $user = $this->getUsers($id);

        $form = $this->createForm(
            UserType::class,
            $user,
            array(
                'action' => $this->generateUrl('user_edit', array('id' => $id)),
            )
        );

        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            $password = $request->request->get('user')['plainPassword']['first'];

            $user->setPlainPassword($password);
            $user->setPassword($password);
            $user->save();

            $encoder = new MessageDigestPasswordEncoder();
            $password = $encoder->encodePassword($password, $user->getSalt());

            $user->setPassword($password);
            $user->save();

            $this->get('session')->getFlashBag()->add(
                'success',
                'User successfully updated.'
            );

            return $this->redirect($this->generateUrl('user_list'));
        }

        return $this->render('user/edit.html.twig', [
            'form' => $form->createView(),
            'user' => $user
        ]);
    }

    /**
     * @Route("/add", name="user_add")
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function createAction(Request $request)
    {
        $user = new User();

        $form = $this->createForm(
            UserType::class,
            $user,
            array(
                'action' => $this->generateUrl('user_add'),
            )
        );

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $name = $request->request->get('user')['username'];
            $password = $request->request->get('user')['plainPassword']['first'];
//            $code = $request->request->get('user')['code'];

            if (!$name || !$password) {
                $this->get('session')->getFlashBag()->set(
                    'error', 'Required field is missing!'
                );
                return $this->redirect($this->generateUrl('user_add'));
            }

            $existing_user = UserQuery::create()
                ->filterByUsername($name)
                ->findOne();

            if ($existing_user) {
                $this->get('session')->getFlashBag()->set(
                    'error', 'Username already used!'
                );
                return $this->redirect($this->generateUrl('user_add'));
            }

            $user = new User();
            $user->setUsername($name);
            $user->setUsernameCanonical($name);
            $user->setPlainPassword($password);
            $user->setPassword($password);
//            $user->setCode($code);
            $user->setEnabled(1);
//            $user->setCreatedAt(new \DateTime());
            $user->save();

            $encoder = new MessageDigestPasswordEncoder();
            $password = $encoder->encodePassword($password, $user->getSalt());

            $user->setPassword($password);
            $user->save();


            $this->get('session')->getFlashBag()->set(
                'success', 'User successfully created!'
            );
            return $this->redirect($this->generateUrl('user_add'));

        }
























//        $user = new User();
//        $form = $this->createForm(UserType::class, $user);
//
//        $form->handleRequest($request);
//
//
//        if ($form->isSubmitted() && $form->isvalid()) {
//            //create the user
////            $password = $encoder->encodePassword($user, $user->getPlainPassword());
////            $user->setPassword($password);
////            $data=$form->getData();
//            $password=$request->request->get('user')['password']['first'];
////            print_r($password);
////            die();
//
////            $userManager = $this->container->get('fos_user.user_manager');
////            $userManager->updateUser($user);
//            $user->setPlainPassword($password);
//            $user->setPassword($password);
//            $user->save();
//            $encoder = new MessageDigestPasswordEncoder();
//            $password = $encoder->encodePassword($password, $user->getSalt());
//
//            $user->setPassword($password);
//            $user->save();
//            print_r($user);
//            return new Response();
//        }

        return $this->render('user/add.html.twig', [
            'form' => $form->createView(),
        ]);
    }


}
