<?php


namespace App\Form;
use App\Model\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{

    public function buildform(FormBuilderInterface $builder,array $options )
    {
        $builder
            ->add('username',TextType::class,[
                'label'=>'Username',
                'required'=>true,'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('email',EmailType::class,[
                'label'=>'Email',
                'required'=>true,'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('plainPassword', RepeatedType::class, array(
                'type' => PasswordType::class,
                'first_options' => array('label' => 'New Password',
                    'attr' => array(
                        'class' => 'form-control'
                    )
                ),
                'second_options' => array('label' => 'Confirm Password',
                    'attr' => array(
                        'class' => 'form-control '
                    )
                ),
                'invalid_message' => 'Password mismatched',

            ))

            ->add( 'submit',SubmitType::class,['label' => 'Submit',
                'attr' => [
                    'class' => 'btn btn-primary',
                    'style' => 'margin-top:1em;'
                ]
                ]);
    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class'=> User::class,
        ]);
    }
}